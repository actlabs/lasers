import sunlight
import MySQLdb
import re
import csv
import ast
import collections
import sys

def convert_keys_to_string(dictionary):
    """Recursively converts dictionary keys to strings."""
    if not isinstance(dictionary, dict):
        return dictionary
    return dict((str(k), convert_keys_to_string(v)) 
        for k, v in dictionary.items())


## Connect to data in local DB
db=MySQLdb.connect(passwd="password",user="root", db="data")
c=db.cursor()
c.execute("SELECT * from bills_of_interest")
data=c.fetchall()
data=list(data)

## Start Cleaning data by fixing session to style needed for Sunlight API
cleandata=[]
for x in data:
    x=list(x)
    count=0
    partialdata=[]
    for y in x:
        if count==2:
            match= re.match(r'([A-Z]+)([0-9]+)',y)
            if match:
                y=match.group(1)+' '+match.group(2)
            else:
                y
        count=count+1
        partialdata.append(y)
    cleandata.append(partialdata)
#print cleandata

## Gather rich data for specific bills/ exclude data without session we will come back to this later
needsession=list()
billlist=list()
for i in cleandata:
    if i[3]=='':
        needsession.append(i)
    else:
        billdata=sunlight.openstates.bill_detail(i[1],i[3],i[2])
        #for b in billdata.keys():
        #    b=b.encode('ascii')
        #    i.append(billdata[b])
        billlist.append(billdata)
print 'done'

## Connect to Shared ACTLabs DB LASERS Schema

lasersdb=MySQLdb.connect(host='mysql.actlabs.org',passwd="A1989access",user="actlabs", db="lasers")
lasersc=lasersdb.cursor()

#iteration=0

## loop through billinfo
for billinfo in billlist:
    
    #iteration=iteration+1
    #print iteration
    
    ## Values to write to bills
    OSid=billinfo[u'id'].encode('utf_8')
    state=billinfo[u'state'].encode('utf_8').upper()
    session=billinfo[u'session'].encode('utf_8')
    title=billinfo[u'title'].encode('utf_8')
    chamber=billinfo[u'chamber'].encode('utf_8')
    createdat=billinfo[u'created_at'].encode('utf_8')
    updatedat=billinfo[u'updated_at'].encode('utf_8')
    billid=billinfo[u'bill_id'].encode('utf_8')
    title=title.replace('"',"'")
    
    ### RUN SQL
    sql = """INSERT INTO bills(OSid, State, Session, Title, ChamberOfOrigin, CreatedAt, UpdatedAt, BillID)
             VALUES ('%s','%s','%s',"%s",'%s','%s','%s','%s')""" % \
             (OSid, state, session, title, chamber, createdat, updatedat, billid)
    try:
        lasersc.execute(sql)
        lasersdb.commit()
    except:
        lasersdb.rollback()
        e = sys.exc_info()
        print 'something broke1', e, sql
    
    ## Values to be writen to alternate_bill_title
    for alttitle in billinfo[u'alternate_titles']:
        OSid
        alttitleis=alttitle.encode('utf_8')
        
        
        ### RUN SQL
        sql = """INSERT INTO alternate_bill_title(OSid, AltTitle)
                 VALUES ('%s','%s')""" % \
                 (OSid, alttitleis)
        try:
            lasersc.execute(sql)
            lasersdb.commit()
        except:
            lasersdb.rollback()
            e = sys.exc_info()
            print 'something broke2', e, sql
    
    ## Values to be writen to all_bill_ids
    for altbillid in billinfo[u'all_ids']:
        OSid
        abillid=altbillid.encode('utf_8')
        #submit
        
        ### RUN SQL
        sql = """INSERT INTO all_bill_ids(OSid, BillID)
                 VALUES ('%s','%s')""" % \
                 (OSid, abillid)
        try:
            lasersc.execute(sql)
            lasersdb.commit()
        except:
            lasersdb.rollback()
            e = sys.exc_info()
            print 'something broke3', e, sql
    
    ## Values to be writen to notable_actions
    firstaction=billinfo[u'action_dates'][u'first'].encode('utf_8')
    try:
        lastaction=billinfo[u'action_dates'][u'last'].encode('utf_8')
    except:
        lastaction=''
    try:
        passedupper=billinfo[u'action_dates'][u'passed_upper'].encode('utf_8')
    except:
        passedupper=''
    try:
        passedlower=billinfo[u'action_dates'][u'passed_lower'].encode('utf_8')
    except:
        passedlower=''
    try:
        signed=billinfo[u'action_dates'][u'signed'].encode('utf_8')
    except:
        signed=''
    
    ### RUN SQL
    sql = """INSERT INTO notable_actions(OSid, First, Last, PassedUpper, PassedLower, Signed)
             VALUES ('%s','%s','%s','%s','%s','%s')""" % \
             (OSid, firstaction, lastaction, passedupper, passedlower, signed)
    try:
        lasersc.execute(sql)
        lasersdb.commit()
    except:
        lasersdb.rollback()
        e = sys.exc_info()
        print 'something broke4', e, sql
    
    
    ## Values to be writen to actions
    for actset in billinfo[u'actions']:
        action=actset[u'action'].encode('utf_8')
        actor=actset[u'actor'].encode('utf_8')
        actdate=actset[u'date'].encode('utf_8')
        action=action.replace('"',"'")
        #submit
        
        ### RUN SQL
        sql = """INSERT INTO actions(OSid, Date, Action, Actor)
                 VALUES ('%s','%s',"%s",'%s')""" % \
                 (OSid, actdate, action, actor)
        try:
            lasersc.execute(sql)
            lasersdb.commit()
        except:
            lasersdb.rollback()
            e = sys.exc_info()
            print 'something broke5', e, sql
    
        ## Values to be writen to action_type
        
        #for actset in billinfo[u'actions']:          #Excluded so that selecting action id is posible
        #get actionid from database
        
        lasersc.execute("""SELECT ActionID FROM actions
                           WHERE OSid='%s' AND Date='%s'""" % \
                           (OSid, actdate))
        actionID=lasersc.fetchone()[0]
        
        for l in actset[u'type']:
            actiontypy=l.encode('utf_8')
            #need to look up autoID for FK
            #submit
            
            ### RUN SQL
            sql = """INSERT INTO action_type(ActionID, Type)
                     VALUES ('%s','%s')""" % \
                     (actionID, actiontypy)
            try:
                lasersc.execute(sql)
                lasersdb.commit()
            except:
                lasersdb.rollback()
                e = sys.exc_info()
                print 'something broke6', e, sql
    
    ## Values to be writen to versions_documents
    for verset in billinfo[u'versions']:
        docid=verset[u'doc_id'].encode('utf_8')
        mimetype=verset[u'mimetype'].encode('utf_8')
        docname=verset[u'name'].encode('utf_8')
        docurl=verset[u'url'].encode('utf_8')
        docname=docname.replace('"',"'")
        
        ### RUN SQL
        sql = """INSERT INTO versions_documents(OSid, DocID, Name, URL, MimeType)
                 VALUES ('%s','%s',"%s",'%s','%s')""" % \
                 (OSid, docid, docname, docurl, mimetype)
        try:
            lasersc.execute(sql)
            lasersdb.commit()
        except:
            lasersdb.rollback()
            e = sys.exc_info()
            print 'something broke', e, sql
            
    for docset in billinfo[u'documents']:
        docid=docset[u'doc_id'].encode('utf_8')
        try:
            docname=docset[u'name'].encode('utf_8')
            docname=docname.replace('"',"'")
        except:
            docname=''
        docurl=docset[u'url'].encode('utf_8')
        mimetype=''
        
        ### RUN SQL
        sql = """INSERT INTO versions_documents(OSid, DocID, Name, URL, MimeType)
                 VALUES ('%s','%s',"%s",'%s','%s')""" % \
                 (OSid, docid, docname, docurl, mimetype)
        try:
            lasersc.execute(sql)
            lasersdb.commit()
        except:
            lasersdb.rollback()
            e = sys.exc_info()
            print 'something broke7', e, sql
        
    
    ## Values to be writen to subjects
    for sub in billinfo[u'subjects']:
        subj=sub.encode('utf_8')
        #submit
    
    ## Values to be writen to sources
    for sub in billinfo[u'sources']:
        subj=sub[u'url'].encode('utf_8')
        
        ### RUN SQL
        sql = """INSERT INTO sources(OSid, SourceURL)
                 VALUES ('%s','%s')""" % \
                 (OSid, subj)
        try:
            lasersc.execute(sql)
            lasersdb.commit()
        except:
            lasersdb.rollback()
            e = sys.exc_info()
            print 'something broke8', e, sql
    
    ## Values to be writen to sponsors
    for spon in billinfo[u'sponsors']:
        try:
            sponname=spon[u'name'].encode('utf_8')
            sponname=sponname.replace('"',"'")
        except:
            sponname=''
        try:
            sponlegid=spon[u'leg_id'].encode('utf_8')
        except:
            sponlegid=''
        try:
            sponcham=spon[u'chamber'].encode('utf_8')
        except:
            sponcham=''
        try:
            spontype=spon[u'type'].encode('utf_8')
        except:
            spontype=''
        try:
            sponotype=spon[u'official_type'].encode('utf_8')
        except:
            sponotype=''
       
        ### RUN SQL
        
        sql = """INSERT INTO sponsors(OSid, Name, Chamber, Legid, Type, OfficialType)
                 VALUES ('%s',"%s",'%s','%s','%s','%s')""" % \
                 (OSid, sponname, sponcham, sponlegid, spontype, sponotype)
        try:
            lasersc.execute(sql)
            lasersdb.commit()
        except:
            lasersdb.rollback()
            e = sys.exc_info()
            print 'something broke9', e, sql
    
    ## Values to be writen to bill_type
    for btype in billinfo[u'type']:
        billtype=btype.encode('utf_8')
        
        ### RUN SQL
        
        sql = """INSERT INTO bill_type(OSid, Type)
                 VALUES ('%s','%s')""" % \
                 (OSid, billtype)
        try:
            lasersc.execute(sql)
            lasersdb.commit()
        except:
            lasersdb.rollback()
            e = sys.exc_info()
            print 'something broke10', e, sql
    
    ## Values to be writen to votes
    for vot in billinfo[u'votes']:
        votechamber=vot[u'chamber'].encode('utf_8')
        voteid=vot[u'vote_id'].encode('utf_8')
        votemotion=vot[u'motion'].encode('utf_8')
        votemotion=votemotion.replace('"',"'")
        votepassed=vot[u'passed']
        votetype=vot[u'type'].encode('utf_8')
        voteyes=vot[u'yes_count']
        voteno=vot[u'no_count']
        voteother=vot[u'other_count']
        
        ### RUN SQL
        
        sql = """INSERT INTO votes(OSid, Chamber, VoteID, Motion, Passed, Type, YesCount, NoCount, OtherCount)
                 VALUES ('%s','%s','%s',"%s",'%s','%s','%s','%s','%s')""" % \
                 (OSid, votechamber, voteid, votemotion, votepassed, votetype, voteyes, voteno, voteother)
        try:
            lasersc.execute(sql)
            lasersdb.commit()
        except:
            lasersdb.rollback()
            e = sys.exc_info()
            print 'something broke11', e, sql
    
        ## Values to be writen to vote_source
        for source in vot[u'sources']:
            soururl=source[u'url'].encode('utf_8')
            
            ### RUN SQL
            
            sql = """INSERT INTO vote_source(VoteID, Source)
                     VALUES ('%s','%s')""" % \
                     (voteid, soururl)
            try:
                lasersc.execute(sql)
                lasersdb.commit()
            except:
                lasersdb.rollback()
                e = sys.exc_info()
                print 'something brok12e', e, sql
    
        ## Values to be writen to ind_votes
        for yesv in vot[u'yes_votes']:
            try:
                voterlegid=yesv[u'leg_id'].encode('utf_8')
            except:
                voterlegid=''
            votername=yesv[u'name'].encode('utf_8')
            votername=votername.replace('"',"'")
            vtype='yes'
            
            ### RUN SQL
            
            sql = """INSERT INTO ind_votes(VoteID, LegID, Name, Vote)
                     VALUES ('%s','%s',"%s",'%s')""" % \
                     (voteid, voterlegid, votername, vtype)
            try:
                lasersc.execute(sql)
                lasersdb.commit()
            except:
                lasersdb.rollback()
                e = sys.exc_info()
                print 'something broke13', e, sql
            
        for nov in vot[u'no_votes']:
            try:
                voterlegid=nov[u'leg_id'].encode('utf_8')
            except:
                voterlegid=''
            votername=nov[u'name'].encode('utf_8')
            votername=votername.replace('"',"'")
            vtype='no'
            
            ### RUN SQL
            
            sql = """INSERT INTO ind_votes(VoteID, LegID, Name, Vote)
                     VALUES ('%s','%s',"%s",'%s')""" % \
                     (voteid, voterlegid, votername, vtype)
            try:
                lasersc.execute(sql)
                lasersdb.commit()
            except:
                lasersdb.rollback()
                e = sys.exc_info()
                print 'something broke14', e, sql
            
        for otherv in vot[u'other_votes']:
            try:
                voterlegid=otherv[u'leg_id'].encode('utf_8')
            except:
                voterlegid=''
            votername=otherv[u'name'].encode('utf_8')
            votername=votername.replace('"',"'")
            vtype='other'
            
            ### RUN SQL
            
            sql = """INSERT INTO ind_votes(VoteID, LegID, Name, Vote)
                     VALUES ('%s','%s',"%s",'%s')""" % \
                     (voteid, voterlegid, votername, vtype)
            try:
                lasersc.execute(sql)
                lasersdb.commit()
            except:
                lasersdb.rollback()
                e = sys.exc_info()
                print 'something broke15', e, sql

print 'assigned variables'

lasersdb.close()


###############################
#
#billinfo.keys()
#keys=list()
#for key in billinfo.keys():
#    print key
#    key=key.encode('utf_8')
#    keys.append(key)
#print keys
#
###############################



####### THE below is used for writing to a CSV#########


#myfile = open('C:/Users/Administrator/Desktop/billalldata.csv','wb')
#wr = csv.writer(myfile)
#for row in allbilldata:
#    newrow=list()
#    for val in row:
#        if type(val)==str:
#            val=val.decode('utf-8')
#            newrow.append(val)
#        elif type(val)==list:
#            newval=list()
#            for rec in val:
#                rec=str(rec)
#                newval.append(rec)
#            newrow.append(newval)
#        elif type(val)==int:
#            newrow.append(val)
#        else:
#            print type(val)
#            newval=list()
#            for rec in val:
#                rec=convert_keys_to_string(rec)
#                #rec=rec.encode('utf-8')
#                newval.append(rec)
#            newrow.append(newval)
#    wr.writerow(newrow)
#    count=+1
#    print count
#myfile.close
#print 'done'
#keylist=list()        
#for g in billinfo.keys():
#    keys=g.encode('utf_8')
#    keylist.append(keys)
#keylist
